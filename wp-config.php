<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'db_toscano_international');
//define('DB_NAME', 'satm_toscano');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');
//define('DB_USER', 'toscanodb');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');
//define('DB_PASSWORD', 'Zg9j5E3c');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');
//define('DB_HOST', 'mysql.sitiosatumedida.com');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Bc)S6o!Y]1|Py[AT^-y3Hcx3#PdboC Re~JGgeEa`n%(lAIPQ25RM(.Og3RH+^3V');
define('SECURE_AUTH_KEY', 'Oc*Q=}z*bIW3F;V^^L~FjqTBspZ,~UJ8UHq~(tBiYJO~l&7.Z9n75c*]Gs^]tg=p');
define('LOGGED_IN_KEY', 'W[#baxxkN6qSm!Yx0V[ lXch:1sM|$Azog.<bv]0@G9;1ka~77KQ%%F2Y-,>_N l');
define('NONCE_KEY', '1htO(0wS%DCsDMuE)Cd96T-!~X7}[*V:/eW,|Sz[k%NgxSLc;E*x JJAthZy9d~Y');
define('AUTH_SALT', '#jroL=,6uAj;}*gwB3(LIZ(UKe@2Wii T%iJ3cqrg)#$jx1v^XpZvQQW;+W!;!Oi');
define('SECURE_AUTH_SALT', '4DYd$9ybwBoLVMTe%gT{)6A8?#8&tcg8xGv4ZB{<hRjc?EAJ^S;;`|U6rvW~V:#K');
define('LOGGED_IN_SALT', '1#MK|0E6JddpsAp$zQ<dw}=}6NQ`sKs?zXL+tm|yBl3W:M|RP_$540c:)@FdMCp&');
define('NONCE_SALT', '183no}N/w,HPx3>Uq7y@wJ:~E]44MKtZGqb7PZ8SXi{oN^-7O8OxP4&6Or;xp0zz');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

