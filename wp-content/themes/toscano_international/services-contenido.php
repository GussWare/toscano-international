<div>
    <div class="layer-wrapper pb-20">
            <?php 
            $services_iconos = get_field("services_iconos", $post_id); 
            if($services_iconos): 
                $services_chunk = array_chunk($services_iconos, 3);

                foreach($services_chunk AS $service_chunk):

                    ?>

                    <div class="row pt-3">

                        <?php 
                        foreach($service_chunk AS $service):
                            
                            ?>

                                <div class="col service-card-2">
                                    <div >
                                        <a href="<?php echo $service['icono_key']; ?>" class="ps2id">
                                            <?php 
                                            if($service['icono_item']):
                                            ?>
                                                <div class="service-icon">
                                                    <i class="<?php echo $service['icono_item']; ?>"></i>
                                                </div>
                                            <?php endif; ?>

                                            <?php 
                                            if($service['icono_titulo']):
                                            ?>
                                                <div class="service-heading"><?php echo $service['icono_titulo']; ?></div>
                                            <?php 
                                            endif;
                                            ?>
                                        </a>
                                    </div>
                                </div>

                            <?php 

                        endforeach;
                        ?>

                    </div>

                    <?php
                    
                endforeach;
            endif;
            ?>

    </div>  

    <div class="mosaico">
        <div>

            <?php 
                $i = 0;
                $color_blanco = "bg-blanco";
                $color_gris = "bg-gris";
                $services_loop = get_field('services_loop', $post_id);

                if($services_loop):
                    foreach($services_loop AS $service):
            ?>
                    <div  class="row">

                        <div id="<?php echo $service['key'];  ?>" class="col-12 col-md-6" >
                            <div class="mosaico-wraper-img">
                                <?php
                                if($service['imagen']):
                                ?>
                                    <img src="<?php echo $service['imagen']; ?>" />
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="col-12 col-md-6">
                            <div class="mosaico-wraper-desc <?php echo (($i % 2) == 0) ? $color_gris : $color_blanco; ?>">
                                    <div class="mosaico-wraper-content">
                                        <?php 
                                        if($service['titulo']):
                                            ?>
                                                <h2>
                                                <?php 
                                                    if($service['icono']):
                                                    ?>
                                                        <i class="<?php echo $service['icono']; ?>"></i>
                                                    <?php
                                                    endif;
                                                    ?>
                                                <?php echo $service['titulo']; ?>
                                                </h2>
                                        <?php endif;  ?>

                                        <?php 
                                        if($service['descripcion']):
                                        ?>
                                                <p><?php echo $service['descripcion']; ?></p>
                                        <?php  endif; ?>
                                    </div>
                            </div>
                        </div>

                    </div>
            <?php  
                        $i++;
                    endforeach;
                endif;
            ?>
        </div>
    </div>
</div>