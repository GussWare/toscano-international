<div class="flexslider slider-wrapper">
    <ul class="slides">
        <li>
            <div class="slider-backgroung-image" style="
            <?php 
            $home_slider_imagen = get_field("home_slider_imagen");
            if($home_slider_imagen):
            ?>
                background-image: url(<?php echo $home_slider_imagen; ?>);
            <?php 
            endif;
            ?>
            ">
                <div class="layer-stretch">
                    <div class="slider-info">
                        <?php 
                        $home_slider_titulo = get_field("home_slider_titulo", $post_id);
                        if($home_slider_titulo):
                        ?>
                            <h1><?php echo $home_slider_titulo; ?></h1>
                        <?php endif; ?>

                        <?php 
                        $home_slider_descripcion = get_field("home_slider_descripcion", $post_id);
                        if($home_slider_descripcion):
                        ?>
                            <p class="animated fadeInDown"><?php echo $home_slider_descripcion; ?></p>
                        <?php endif; ?>

                        <?php 
                        $home_slider_boton = get_field('home_slider_boton', $post_id);
                        $home_slider_boton_url = get_field('home_slider_boton_url', $post_id);

                        if($home_slider_boton && $home_slider_boton_url):
                        ?>
                            <div class="slider-button">
                                <a class="btn btn-cuadrado-transparente" href= "
                                    <?php 
                                    if($home_slider_boton_url):
                                        echo home_url() . $home_slider_boton_url;
                                    endif;
                                    ?>
                                ">
                                    <?php echo $home_slider_boton; ?>
                                    <i class="fa fa-chevron-right"></i>
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </li> 

    </ul>
</div>
        