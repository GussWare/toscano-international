<div class="col-md-6 col-lg-4">
    <div class="blog-card">
        <?php the_post_thumbnail('medium-large'); ?>
        <div>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <div class="blog-meta">
                <p><i class="icon-user"></i><span><?php the_author(); ?></span></p>
                <p><i class="icon-clock"></i><span><?php the_date(); ?></span></p>
                <p><i class="icon-bubble"></i><span><?php comments_number( ); ?></span></p>
            </div>
            
            <?php echo helper_custom_excerpt(); ?>

            <a href="<?php the_permalink();?>"><span>Read More</span><i class="icon-arrow-right"></i></a>
        </div>
    </div>
</div>