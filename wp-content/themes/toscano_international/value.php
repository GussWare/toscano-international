<?php
/**
 * Template Name: Value
 * Description: Template tipo Value page
 */

get_header();

$post_id = get_the_ID();
?>

<!-- Start Slider Section -->
<div id="slider" class="slider-dark">
    <?php include( locate_template('value-slider.php') ) ?>
</div>
<!-- End Slider Section -->

<!-- Start services Section -->
<div class="value">
    <?php include( locate_template('value-contenido.php') ) ?>
</div>
<!-- End services Section -->

<?php get_footer(); ?>