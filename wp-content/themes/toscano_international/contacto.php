<?php
/**
 * Template Name: Contacto
 * Description: Template tipo Contacto page
 */

get_header();

$post_id = get_the_ID();

$mapa_toscano_international = get_field('mapa_toscano_international', $post_id);
$mapa_toscano_mexico = get_field('mapa_toscano_mexico', $post_id);
?>

<div class="contacto">
<!-- Start page Title Section -->
<div class="page-ttl" style="
    <?php 
    $contacto_slider_imagen = get_field("contacto_slider_imagen", $post_id);
    if($contacto_slider_imagen):
    ?>
        background-image: url(<?php echo $contacto_slider_imagen; ?>);
    <?php endif; ?>
">
</div><!-- End page Title Section -->

<br />
<br />
<br />

<div class="row">
    <div class="col-12 col-md-6">
        <div class="page-ttl-container">
            <div class="titulo-contacto">
                    <?php 
                    $contacto_slider_titulo = get_field("contacto_slider_titulo", $post_id);
                    if($contacto_slider_titulo):
                    ?>
                        <h1><?php echo $contacto_slider_titulo; ?></h1>
                    <?php endif; ?>
            </div>
        </div>
        <?php echo do_shortcode('[contact-form-7 id="4"]'); ?>
    </div>

    <div class="col-12 col-md-6">
       

        <?php 
        $titulo_direccion_internacional = get_field("titulo_direccion_internacional", $post_id);
        if($titulo_direccion_internacional): 
        ?>
            <h2><?php echo $titulo_direccion_internacional; ?></h2>
        <?php endif; ?>

        <?php 
        $direccion_internacional = get_field("direccion_internacional", $post_id);
        if($direccion_internacional):
        ?>
        <p><?php echo $direccion_internacional; ?></p>
        <?php endif; ?>

        <?php if($mapa_toscano_international): ?>
         <div class="acf-map">
             <div class="marker" data-lat="<?php echo $mapa_toscano_international['lat']; ?>" data-lng="<?php echo $mapa_toscano_international['lng']; ?>"></div>
         </div>
         <?php endif; ?>


        <?php 
        $titulo_direccion_mexico = get_field("titulo_direccion_mexico", $post_id);
        if($titulo_direccion_mexico): 
        ?>
            <h2><?php echo $titulo_direccion_mexico; ?></h2>
        <?php endif; ?>

        <?php 
        $direccion_mexico = get_field("direccion_mexico", $post_id);
        if($direccion_mexico):
        ?>
        <p><?php echo $direccion_mexico; ?></p>
        <?php endif; ?>

        <?php if($mapa_toscano_mexico): ?>
         <div class="acf-map">
             <div class="marker" data-lat="<?php echo $mapa_toscano_mexico['lat']; ?>" data-lng="<?php echo $mapa_toscano_mexico['lng']; ?>"></div>
         </div>
        <?php endif; ?>
    </div>
</div>

<br />
<br />
<br />

</div>

<?php get_footer(); ?>