<div class="flexslider slider-wrapper">
    <ul class="slides">
        <li>
            <div class="slider-backgroung-image" style="
            <?php 
            $value_slider_imagen = get_field("value_slider_imagen");
            if($value_slider_imagen):
            ?>
                background-image: url(<?php echo $value_slider_imagen; ?>);
            <?php 
            endif;
            ?>
            ">
                <div class="layer-stretch">
                    <div class="slider-info">
                        <?php 
                        $value_slider_titulo = get_field("value_slider_titulo", $post_id);
                        if($value_slider_titulo):
                        ?>
                            <h1><?php echo $value_slider_titulo; ?></h1>
                        <?php endif; ?>

                        <?php 
                        $value_slider_descripcion = get_field("value_slider_descripcion", $post_id);
                        if($value_slider_descripcion):
                        ?>
                            <p class="animated fadeInDown"><?php echo $value_slider_descripcion; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li> 

    </ul>
</div>
        