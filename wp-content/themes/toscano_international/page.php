<?php
/**
 * The page
 *
 */
get_header(); ?>

<div id="primary" class="">
	<main id="" class="">
		<?php
		while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', 'page' ); ?>
		<?php
		endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->



<?php get_footer(); ?>
