<div class="flexslider slider-wrapper">
    <ul class="slides">
        <li>
            <div class="slider-backgroung-image" style="
            <?php 
            $services_slider_imagen = get_field("services_slider_imagen", $post_id);
            if($services_slider_imagen):
            ?>
                background-image: url(<?php echo $services_slider_imagen; ?>);
            <?php 
            endif;
            ?>
            ">
                <div class="layer-stretch">
                    <div class="slider-info">
                        <?php 
                        $services_slider_titulo = get_field("services_slider_titulo", $post_id);
                        if($services_slider_titulo):
                        ?>
                            <h1><?php echo $services_slider_titulo; ?></h1>
                        <?php endif; ?>

                        <?php 
                        $services_slider_descripcion = get_field("services_slider_descripcion", $post_id);
                        if($services_slider_descripcion):
                        ?>
                            <p class="animated fadeInDown"><?php echo $services_slider_descripcion; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li> 

    </ul>
</div>
        