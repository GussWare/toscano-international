<?php
/**
 * Template Name: Aboutus
 * Description: Template tipo Aboutus page
 */

get_header();

$post_id = get_the_ID();
?>

<!-- Start Slider Section -->
<div id="slider" class="slider-dark">
    <?php include( locate_template('aboutus-slider.php') ) ?>
</div>
<!-- End Slider Section -->

<!-- Start Conteido Section -->
<div class="mosaico">
    <?php include( locate_template('aboutus-contenido.php') ) ?>
</div>
<!-- End Conteido Section -->

<?php get_footer(); ?>