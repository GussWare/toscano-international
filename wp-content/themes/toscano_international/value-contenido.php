<div class="feature">
    <div class="row m-0">


        <div class="col-12 col-md-4 feature-block">
            <div class="block">
                <?php 
                $value_help_icono = get_field("value_help_icono", $post_id);
                if($value_help_icono):
                ?>
                    <i class="<?php echo $value_help_icono; ?>"></i>
                <?php endif; ?>

                <?php 
                $value_help_descripcon = get_field("value_help_descripcon", $post_id);
                if($value_help_descripcon):
                ?>
                <span><?php echo $value_help_descripcon; ?></span>
                <?php endif; ?>
            </div>
        </div>


        <div class="col-12 col-md-4 feature-block">
            <div class="block">
                <?php 
                $value_clock_icon = get_field("value_clock_icon", $post_id);
                if($value_clock_icon):
                ?>
                    <i class="<?php echo $value_clock_icon; ?>"></i>
                <?php endif; ?>

                <?php 
                $value_clock_descripcion = get_field("value_clock_descripcion", $post_id);
                if($value_clock_descripcion):
                ?>
                <span><?php echo $value_clock_descripcion; ?></span>
                <?php endif; ?>
            </div>
        </div>


        <div class="col-12 col-md-4 feature-block">
            <div class="block">
                <?php 
                $value_people_icon = get_field("value_people_icon", $post_id);
                if($value_people_icon):
                ?>
                    <i class="<?php echo $value_people_icon; ?>"></i>
                <?php endif; ?>

                <?php 
                $value_people_descripcion = get_field("value_people_descripcion", $post_id);
                if($value_people_descripcion):
                ?>
                <span><?php echo $value_people_descripcion; ?></span>
                <?php endif; ?>
            </div>
        </div>
        
    </div>
</div><!-- End of Service Section -->