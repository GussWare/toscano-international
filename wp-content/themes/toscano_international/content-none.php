<div class="layer-stretch">
    <div class="layer-wrapper pb-3">

        <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
            <div class="layer-sub-ttl">¿Listo para tu primera publicación? <a href="<?php echo esc_url( admin_url( 'post-new.php' ) ); ?>">Inicia aquí</a></div>
        <?php elseif ( is_search() ) : ?>
            <div class="layer-sub-ttl">Lo sentimos, no encontramos resultados con estos terminos de búsqueda. Por favor intenta de nuevo con terminos diferentes.</div>
        <?php 
            get_search_form();
            else : 
        ?>
                <div class="layer-sub-ttl">Parece que no podemos encontrar lo que estas buscando. Quizas la búsqueda pueda ayudar.</div>
        <?php
         endif;
          ?>

        
    </div>
</div>