<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); 
?>

<!-- Start Not Found Section -->
<div class="notfound-wrapper">
	<h1 class="notfound-ttl">404</h1>
	<p class="notfound-tag-1">PÁGINA NO ENCONTRADA!!!</p>
	<span class="notfound-tag-2">Parece que no podemos encontrar lo que estas buscando.<br /> Quizas la búsqueda pueda ayudar.</span>
	
	<div style="margin:0 auto;">
		<?php echo get_search_form(); ?>
	</div>
	
</div><!-- End Not Found Section -->

<br />
<br />
<br />
<br />

<?php get_footer(); ?>