
<!-- Start Footer Section -->
<footer id="footer">
        <div class="layer-stretch">
            <!-- Start main Footer Section -->
            <div class="row layer-wrapper">
                <div class="col-md-4 footer-block">
                    <?php 
                    $seccion_1_titulo = get_field("seccion_1_titulo", "option");
                    if($seccion_1_titulo):
                    ?>
                        <div class="footer-ttl"><p><?php echo $seccion_1_titulo; ?></p></div>
                    <?php endif; ?>

                    <?php 
                    $seccion_1_descripcion = get_field("seccion_1_descripcion", "option");
                    if($seccion_1_descripcion):
                    ?>
                        <div class="footer-container footer-a">      
                            <?php echo $seccion_1_descripcion; ?>                      
                        </div>
                    <?php  
                    endif;
                    ?>
                </div>

                <div class="col-md-8 footer-block">

                    <div class="row">
                        <div class="col-12">
                            <div class="footer-ttl"><p>Contact Us</p></div>
                        </div>
                    </div>
                    

                    <div class="row" style="margin-top:-20px;">
                        <div class="col-md-6">
                        <?php 
                        
                            $seccion_2_descripcion = get_field("seccion_2_descripcion", "option");
                            if($seccion_2_descripcion):
                            ?>
                                <div class="footer-container footer-a">      
                                    <?php echo $seccion_2_descripcion; ?>                      
                                </div>
                            <?php  
                            endif;
                            ?>
                        </div>

                        <div class="col-md-6">
                        <?php 
                                
                                $seccion_3_descripcion = get_field("seccion_3_descripcion", "option");
                                if($seccion_3_descripcion):
                                ?>
                                    <div class="footer-container footer-a">      
                                        <?php echo $seccion_3_descripcion; ?>                      
                                    </div>
                                <?php  
                                endif;
                                ?>
                        </div>
                    </div>
                
                </div>

            </div>
        </div><!-- End main Footer Section -->
        <!-- Start Copyright Section -->
        <div id="copyright">
            <div class="layer-stretch">
                <?php 
                $texto_copy = get_field("texto_copy", "option"); 
                if($texto_copy):
                ?>
                    <div class="paragraph-medium paragraph-white"><?php echo $texto_copy; ?></div>
                <?php endif; ?>
            </div>
        </div><!-- End of Copyright Section -->
    </footer><!-- End of Footer Section -->
</div>    

    <!-- **********Included Scripts*********** -->
    <?php wp_footer();?>
    
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/jquery/jquery-2.1.4.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/popper/popper.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/modernizr/modernizr.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/animateheading/animateheading.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/material/material.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/material/mdl-selectfield.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/flexslider/jquery.flexslider.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/owl_carousel/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/scrolltofixed/jquery-scrolltofixed.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/magnific_popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/waypoints/jquery.waypoints.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/counterup/jquery.counterup.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/masonry_pkgd/masonry.pkgd.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/smoothscroll/smoothscroll.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/plugin/styleswitcher/jQuery.style.switcher.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>
    
</body>
</html>