<div class="flexslider slider-wrapper">
    <ul class="slides">
        <li>
            <div class="slider-backgroung-image" style="
            <?php 
            $about_us_slider_imagen = get_field("about_us_slider_imagen");
            if($about_us_slider_imagen):
            ?>
                background-image: url(<?php echo $about_us_slider_imagen; ?>);
            <?php 
            endif;
            ?>
            ">
                <div class="layer-stretch">
                    <div class="slider-info">
                        <?php 
                        $about_us_slider_titulo = get_field("about_us_slider_titulo", $post_id);
                        if($about_us_slider_titulo):
                        ?>
                            <h1><?php echo $about_us_slider_titulo; ?></h1>
                        <?php endif; ?>

                        <?php 
                        $about_us_slider_descripcion = get_field("about_us_slider_descripcion", $post_id);
                        if($about_us_slider_descripcion):
                        ?>
                            <p class="animated fadeInDown"><?php echo $about_us_slider_descripcion; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li> 

    </ul>
</div>
        