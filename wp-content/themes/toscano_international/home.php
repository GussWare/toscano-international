<?php
/**
 * Template Name: Home
 * Description: Template tipo Home page
 */

get_header();

$post_id = get_the_ID();
?>

<!-- Start Slider Section -->
<div id="slider" class="slider-dark ">
    <?php include( locate_template('home-slider.php') ) ?>
</div>
<!-- End Slider Section -->

<!-- Start Conteido Section -->
<div class="mosaico">
    <?php include( locate_template('home-contenido.php') ) ?>
</div>
<!-- End Conteido Section -->

<?php get_footer(); ?>