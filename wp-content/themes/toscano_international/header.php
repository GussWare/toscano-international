<!doctype html>
<html <?php language_attributes() ?>>

  <head>
    <title>
        <?php wp_title( ' | ', true, 'right' ); ?><?php bloginfo( 'name' ); ?>
    </title>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo( 'charset' ) ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/material/material.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/material/mdl-selectfield.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/animateheading/animateheading.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/owl_carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/animate/animate.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/magnific_popup/magnific-popup.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/plugin/flexslider/flexslider.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/fontawesome/css/all.css" />

    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

    <?php wp_head(); ?>
  </head>

  <body>

    <div class="wrapper">
      <!-- Start Header Section -->
      <header id="header" class="header-dark">
            <div class="layer-stretch hdr">
                <div class="tbl animated fadeInDown">
                    <div class="tbl-row">
                        <!-- Start Header Logo Section -->
                        <div class="tbl-cell hdr-logo">
                            <a href="<?php echo get_home_url(); ?>"><img id="logo" data-template-url="<?php echo get_template_directory_uri(); ?>" src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo_claro.png" alt=""></a>
                        </div><!-- End Header Logo Section -->
                        <div class="tbl-cell hdr-menu">
                            <!-- Start Menu Section -->

                            <?php 
                            wp_nav_menu( array( 
                                'theme_location' => 'home_principal', 
                                'container' => '', 
                                'menu_class' => 'menu', 
                                'menu_id' => 'menu-principal'
                                ) );
                            ?>

                            <div id="menu-bar"><a><i class="fa fa-bars"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="search-bar animated zoomIn">
                    <div class="search-content">
                        <div class="search-input">
                            <input type="text" placeholder="Enter your text ....">
                            <button class="search-btn"><i class="icon-magnifier"></i></button>
                        </div>
                    </div>
                    <div class="search-close"><i class="icon-close"></i></div>
                </div>
            </div>
        </header><!-- End Header Section -->