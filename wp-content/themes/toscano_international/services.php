<?php
/**
 * Template Name: Services
 * Description: Template tipo Services page
 */

get_header();

$post_id = get_the_ID();
?>

<!-- Start Slider Section -->
<div id="slider" class="slider-dark">
    <?php include( locate_template('services-slider.php') ) ?>
</div>
<!-- End Slider Section -->

<!-- Start services Section -->
<div class="services">
    <?php include( locate_template('services-contenido.php') ) ?>
</div>
<!-- End services Section -->

<?php get_footer(); ?>