<div>
    <div class="row">
        <div class="mosaico-wraper-img col-12 col-md-6">
            <?php
            $home_partner_imagen = get_field("home_partner_imagen", $post_id);
            if($home_partner_imagen):
            ?>
                <img src="<?php echo $home_partner_imagen; ?>" />
            <?php endif; ?>
        </div>

        <div class="mosaico-wraper-desc col-12 col-md-6">

            <div class="mosaico-wraper-content">
            <?php 
            $home_partner_titulo = get_field("home_partner_titulo", $post_id);
            if($home_partner_titulo):
            ?>
                <h2><?php echo $home_partner_titulo; ?></h2>
            <?php endif; ?>

            <?php 
            $home_partner_descripcion = get_field("home_partner_descripcion", $post_id);
            if($home_partner_descripcion):
            ?>
                <p><?php echo $home_partner_descripcion; ?></p>
            <?php endif; ?>

            <?php 
            $home_partner_boton_url = get_field("home_partner_boton_url", $post_id);
            $home_partner_boton = get_field("home_partner_boton", $post_id);

            if($home_partner_boton_url && $home_partner_boton):
            ?>
                <a href="<?php echo home_url() . $home_partner_boton_url ?>" class="btn btn-cuadrado-transparente-hover-gris"><?php echo $home_partner_boton; ?> <i class="fa fa-chevron-right"></i></a>
            <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="mosaico-wraper-desc col-12 col-md-6">
            <div class="mosaico-wraper-content">
            <?php 
            $home_clients_titulo = get_field("home_clients_titulo", $post_id);
            if($home_clients_titulo):
            ?>
                <h2><?php echo $home_clients_titulo; ?></h2>
            <?php endif; ?>

            <?php 
            $home_clients_descripcion = get_field("home_clients_descripcion", $post_id);
            if($home_clients_descripcion):
            ?>
                <p><?php echo $home_clients_descripcion; ?></p>
            <?php endif; ?>

            <?php 
            $home_clients_boton = get_field("home_clients_boton", $post_id);
            $home_clienets_boton_url = get_field("home_clienets_boton_url", $post_id);

            if($home_clienets_boton_url && $home_clients_boton):
                ?>
                    <a href="<?php echo home_url() . $home_clienets_boton_url ?>" class="btn btn-cuadrado-transparente-hover-gris"><?php echo $home_clients_boton; ?> <i class="fa fa-chevron-right"></i></a>
                <?php endif; ?>
            </div>
        </div>

        <div class="mosaico-wraper-img col-12 col-md-6">
            <?php
            $home_clients_imagen = get_field("home_clients_imagen", $post_id);
            if($home_clients_imagen):
            ?>
                <img src="<?php echo $home_clients_imagen; ?>" />
            <?php endif; ?>
        </div>
    </div>
</div>