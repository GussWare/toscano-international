<div>

    <!--   START OUR Greatest  -->
    <div class="row">
        <div class="mosaico-wraper-img col-12 col-md-6">
            <?php
            $our_greatest_imagen = get_field("our_greatest_imagen", $post_id);
            if($our_greatest_imagen):
            ?>
                <img src="<?php echo $our_greatest_imagen; ?>" />
            <?php endif; ?>
        </div>

        <div class="mosaico-wraper-desc col-12 col-md-6">

            <div class="mosaico-wraper-content">
            <?php 
            $our_greatest_titulo = get_field("our_greatest_titulo", $post_id);
            if($our_greatest_titulo):
            ?>
                <h2><?php echo $our_greatest_titulo; ?></h2>
            <?php endif; ?>

            <?php 
            $our_greatest_descripcion = get_field("our_greatest_descripcion", $post_id);
            if($our_greatest_descripcion):
            ?>
                <p><?php echo $our_greatest_descripcion; ?></p>
            <?php endif; ?>

            <?php 
            $our_greatest_boton_url = get_field("our_greatest_boton_url", $post_id);
            $our_greatest_boton_titulo = get_field("our_greatest_boton_titulo", $post_id);

            if($our_greatest_boton_titulo && $our_greatest_boton_url):
            ?>
                <a href="<?php echo home_url() . $our_greatest_boton_url ?>" class="btn btn-cuadrado-transparente-hover-gris"><?php echo $our_greatest_boton_titulo; ?> <i class="fa fa-chevron-right"></i></a>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <!--   END OUR Greatest  -->


    <!--   START OUR Tailored   -->
    <div class="row">
        <div class="mosaico-wraper-desc col-12 col-md-6">
            <div class="mosaico-wraper-content">
            <?php 
            $our_tailored_titulo = get_field("our_tailored_titulo", $post_id);
            if($our_tailored_titulo):
            ?>
                <h2><?php echo $our_tailored_titulo; ?></h2>
            <?php endif; ?>

            <?php 
            $our_tailored_descripcion = get_field("our_tailored_descripcion", $post_id);
            if($our_tailored_descripcion):
            ?>
                <p><?php echo $our_tailored_descripcion; ?></p>
            <?php endif; ?>

            <?php 
            $our_tailored_boton_titulo = get_field("our_tailored_boton_titulo", $post_id);
            $our_tailored_boton_url = get_field("our_tailored_boton_url", $post_id);

            if($our_tailored_boton_url && $our_tailored_boton_titulo):
                ?>
                    <a href="<?php echo home_url() . $our_tailored_boton_url ?>" class="btn btn-cuadrado-transparente-hover-gris"><?php echo $our_tailored_boton_titulo; ?> <i class="fa fa-chevron-right"></i></a>
                <?php endif; ?>
            </div>
        </div>

        <div class="mosaico-wraper-img col-12 col-md-6">
            <?php
            $our_tailored_imagen = get_field("our_tailored_imagen", $post_id);
            if($our_tailored_imagen):
            ?>
                <img src="<?php echo $our_tailored_imagen; ?>" />
            <?php endif; ?>
        </div>
    </div>
    <!--   END OUR Tailored   -->



    <!--   START OUR Services   -->
    <div class="row">
        <div class="mosaico-wraper-img col-12 col-md-6">
            <?php
            $our_services_imagen = get_field("our_services_imagen", $post_id);
            if($our_services_imagen):
            ?>
                <img src="<?php echo $our_services_imagen; ?>" />
            <?php endif; ?>
        </div>

        <div class="mosaico-wraper-desc col-12 col-md-6">

            <div class="mosaico-wraper-content">
            <?php 
            $our_services_titulo = get_field("our_services_titulo", $post_id);
            if($our_services_titulo):
            ?>
                <h2><?php echo $our_services_titulo; ?></h2>
            <?php endif; ?>

            <?php 
            $our_services_descripcion = get_field("our_services_descripcion", $post_id);
            if($our_services_descripcion):
            ?>
                <p><?php echo $our_services_descripcion; ?></p>
            <?php endif; ?>

            <?php 
            $our_services_boton_url = get_field("our_services_boton_url", $post_id);
            $our_services_boton_titulo = get_field("our_services_boton_titulo", $post_id);

            if($our_services_boton_url && $our_services_boton_titulo):
            ?>
                <a href="<?php echo home_url() . $our_services_boton_url ?>" class="btn btn-cuadrado-transparente-hover-gris"><?php echo $our_services_boton_titulo; ?> <i class="fa fa-chevron-right"></i></a>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <!--   END OUR Services   -->



    <!--   START OUR Leadership    -->
    <div class="row">
        
        <div class="mosaico-wraper-desc col-12 col-md-6">

            <div class="mosaico-wraper-content">
            <?php 
            $our_leadership_titulo = get_field("our_leadership_titulo", $post_id);
            if($our_leadership_titulo):
            ?>
                <h2><?php echo $our_leadership_titulo; ?></h2>
            <?php endif; ?>

            <?php 
            $our_leadership_descripcion = get_field("our_leadership_descripcion", $post_id);
            if($our_leadership_descripcion):
            ?>
                <p><?php echo $our_leadership_descripcion; ?></p>
            <?php endif; ?>


            <?php 
            $our_leadership_boton_url = get_field("our_leadership_boton_url", $post_id);
            $our_leadership_boton_titulo = get_field("our_leadership_boton_titulo", $post_id);

            if($our_leadership_boton_titulo && $our_leadership_boton_url):
            ?>
                <a href="<?php echo home_url() . $our_leadership_boton_url ?>" class="btn btn-cuadrado-transparente-hover-gris"><?php echo $our_leadership_boton_titulo; ?> <i class="fa fa-chevron-right"></i></a>
            <?php endif; ?>
            </div>
        </div>

        <div class="mosaico-wraper-img col-12 col-md-6">
            <?php
            $our_leadership_imagen = get_field("our_leadership_imagen", $post_id);
            if($our_leadership_imagen):
            ?>
                <img src="<?php echo $our_leadership_imagen; ?>" />
            <?php endif; ?>
        </div>
    </div>
    <!--   END OUR Leadership   -->


</div>