var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');

var dir_scss = './wp-content/themes/toscano_international/assets/sass/';
var dir_css = './wp-content/themes/toscano_international/';
var dir_js = './wp-content/themes/toscano_international/assets/js/';

gulp.task('sass', function () {
    gulp.src(dir_scss + '*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest(dir_css))

    var files = [
        
    ];

    
});

gulp.task('default', ['sass']);